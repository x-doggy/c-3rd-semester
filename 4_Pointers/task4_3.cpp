/*
 * (с) 2014, Владимир Стадник, программа 4.3
 * Написано и протестировано в Fedora GNU/Linux
 * ********************************************
 * Создание конструкции вида:
 * p -> [o] -> []
 *      ...
 *      [o] -> []
 * ********************************************
 * Обнулите все числа в квадратиках.
 * Поместите в первый и последний элементы число 2
 * и выведите массив на экран.
 * Удалите все динамические объекты.
*/

#include <iostream>
#include <limits>
using namespace std;


int main(int argc, char **argv) {

    cout << "Enter a number of arrays: ";
    int size;
    cin >> size;

    double **p = new double*[size];
    for (int i=0; i<size; i++) {
        p[i] = new double;
        *p[i] = 0;
    }
    *p[0] = *p[size-1] = 2;
    
    cout << "Array:" << endl;
    for (int i=0; i<size; i++)
        cout << *p[i] << "  ";
    
    cout << endl << "Deleting dynamic structures..." << endl;
    for (int i=0; i<size; i++)
        delete p[i];
    delete[] p;
    
    return 0;
}
