/*
 * (с) 2014, Владимир Стадник, программа 4.2
 * Написано и протестировано в Fedora GNU/Linux
 * ********************************************
 * Создание конструкции вида:
 * p -> o -> [a b c d ...]
 * Длина массива вводится предварительно с клавиатуры.
 * Обнулите все элементы массива. Поместите в первый и последний
 * элементы число 2 и выведите массив на экран.
 * Удалите все динамические объекты.
*/


#include <iostream>
using namespace std;


int main(int argc, char **argv) {

    cout << "-> ";
    int size;
    cin >> size;

    double **p = new double *;
    *p = new double[size];

    for (int i=0; i<size; i++) {
        *(*p+i) = (double) i + 1;
    }
    **p = *(*p + size - 1) = 2;
    
    cout << "<- ";
    for (int i=0; i<size; i++)
        cout << *(*p+i) << "  ";
    cout << endl;

    delete[] *p;
    delete p;
    
    return 0;
}
