/*
 * (с) 2014, Владимир Стадник, программа 4.5
 * Написано и протестировано в Fedora GNU/Linux
 * ********************************************
 * Создайте динамический массив указателей на структуры типа Sample.
 * Выделите каждому указателю массива память под одну структуру.
 * Далее сделайте действия из предыдущей задачи.
 */

#include <iostream>
using namespace std;


struct Sample {
    char c;
    double x;
    int *p;
};

int main(int argc, char **argv) {

    cout << "Enter max: ";
    int size;
    cin >> size;

    Sample **smp = new Sample*[size];

    for (int i=0; i<size; i++) {
        smp[i] = new Sample;
        smp[i]->p = new int[size];
        
        cout << "i=" << i << endl;
        cout << "Array:" << endl;
        for (int j=0; j<size; j++) {
            smp[i]->p[j] = i;
            cout << smp[i]->p[j] << "  ";
        }
        smp[i]->c = static_cast<char>(i+48);
        smp[i]->x = i*9;
        cout << "\nc = " << smp[i]->c << "\tx = " << smp[i]->x << endl;
    }
    
    for (int i=0; i<size; i++) {
        delete[] smp[i]->p;
        delete[] smp[i];
    }
    
    delete[] smp;
    
    return 0;
}
