/*
 * (с) 2014, Владимир Стадник, программа 4.4
 * Написано и протестировано в Fedora GNU/Linux
 * ********************************************
 * Объявите переменную типа Sample.
 * Сделайте так, чтобы указатель p указывал на начало массива из n элементов
 * (число n вводится с клавиатуры).
 * Присвойте какие-нибудь значения полям c и x, а также элементам массива.
 * Выведите все эти данные на экран.
 */

#include <iostream>
using namespace std;


struct Sample {
    char c;
    double x;
    int *p;
};


int main(int argc, char **argv) {

    Sample smp;
    
    cout << "Enter n: ";
    int n;
    cin >> n;
    
    smp.p = new int[n];
    
    for (int i=0; i<n; i++)
        smp.p[i] = i;
    
    smp.p[n-1] = 5;
    smp.c = '+';
    smp.x = 100.01;

    cout << "Array:" << endl;
    for (int i=0; i<n; i++)
        cout << smp.p[i] << ", ";

    cout << "\nDeleting dynamic structures..." << endl;
    delete[] smp.p;

    return 0;
}
