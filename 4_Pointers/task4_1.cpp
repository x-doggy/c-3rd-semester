/*
 * (с) 2014, Владимир Стадник, программа 4.1
 * Написано и протестировано в Fedora GNU/Linux
 * ********************************************
 * Создание конструкции вида:
 * p -> q -> 2
*/


#include <iostream>
using namespace std;


int main(int argc, char **argv) {

    double **p = new double *;
    *p = new double;
    **p = 2;
    
    cout << "**p=" << **p << endl;
    
    delete *p;
    delete p;

    return 0;
}
