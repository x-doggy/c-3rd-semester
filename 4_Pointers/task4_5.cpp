/*
 * (с) 2014, Владимир Стадник, программа 4.5
 * Написано и протестировано в Fedora GNU/Linux
 * ********************************************
 * Создайте динамический массив элементов типа Sample.
 * Сделайте так, чтобы поле p каждой структуры указывал на начало массива
 * из n элементов (число n вводится с клавиатуры).
 * Для i-го элемента массива структур инициализируйте элементы массива p числом i.
 * Распечатайте все структуры.
 */

#include <iostream>
using namespace std;


struct Sample {
    char c;
    double x;
    int *p;
};


int main(int argc, char **argv) {

    cout << "Enter size: ";
    int size;
    cin >> size;

    Sample *smp = new Sample[size];
    
    for (int i=0; i<size; i++) {
        smp[i].p = new int[size];

        cout << "\ni=" << i << endl;
        cout << "Array:\t";
        for (int j=0; j<size; j++) {
            smp[i].p[j] = i;
            cout << smp[i].p[j] << "  ";
        }
        smp[i].c = static_cast<char>(i+48);
        smp[i].x = i*9;
        cout << "\nc = " << smp[i].c << "\tx = " << smp[i].x << endl;
    }
    
    for (int i=0; i<size; i++)
        delete[] smp[i].p;
    delete[] smp;

    return 0;
}
