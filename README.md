# Программы 3-го семестра на C++

Все программы можно собрать разом при помощи систем сборки [CMake](https://cmake.org/) + [Ninja](https://ninja-build.org/).

Если ваша IDE не умеет в CMake, то в общем случае собрать проект можно так:

```cmake
mkdir build
cd build
cmake -GNinja ../
cmake --build .
```

Собранные исполняемые файлы находятся теперь в подпапках папки `build`.

Проект возможно собрать в среде NetBeans.

Программы написаны без использования `try` / `catch`. Но это поправимо.

**Пример.** Если есть код типа:

```
double *a = new double;
```

То его можно обернуть в `try` / `catch`:

```
double *a = nullptr;
try {
    a = new double;
}
catch (std::bad_alloc const &err) {
    std::cerr << err.what() << std::endl;
    return -1;
}
```

Корректность ввода правильных типов данных в `cin` можно также обработать:

```
#include <limits>

int a;
if (!(cin >> a)) {
    cin.clear();
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    continue;            // или break;
}
```

И ещё. Программа 5.13 необычна своим Unix-like походом к пользователю.

**Писать как здесь — си-шный код средствами c++ — плохо!**
