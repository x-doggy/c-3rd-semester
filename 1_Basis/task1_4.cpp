/*
 * (с) 2014, Владимир Стадник, программа 1.4
 * Написано и протестировано в Fedora GNU/Linux
 * ********************************************
 * Программа, которая вводит три вещественных числа,
 * понимая их как коэффициенты квадратного уравнения.
 * Программа производит анализ этого уравнения на предмет
 * количества корней и находит корни.
 */

#include <iostream>
#include <cmath>
using namespace std;


void quadraticEq(double a, double b, double c) {

    double discr, x1, x2;

    if (a == 0 && b == 0 && c == 0)
        cout << "Equation has infinity solutions" << endl;

    else if (a == 0 && b == 0 && c != 0)
        cout << "Equation hasn\'t solutions!" << endl;

    else if (a == 0 && b != 0  && c != 0) {
        x1 = -c / b;
        x2 = x1;
        cout << "x=" << x1 << endl;
    }

    else if (a != 0 && b == 0 && c != 0 && a*c < 0) {
        x1 = sqrt(c/a);
        x2 = -x1;
        cout << "x1=" << x1 << "\nx2=" << x2 << endl;
    }

    else if (a != 0 && c == 0) {
        x1 = 0;
        x2 = -b / a;
        cout << "x1=" << x1 << "\nx2=" << x2 << endl;
    }

    else if (a == 0 && b != 0 && c == 0) {
        cout << "x = " << (x1 = x2 = 0) << endl;
    } else {
        discr = b * b - 4 * a * c;
        cout << "Discriminant=" << discr << endl;

        if (discr < 0)
            cout << "[Warn] No solutions: discriminant is negative!" << endl;
        else if (discr == 0) {
            x1 = -b / 2 / a;
            x2 = x1;
            cout << "x=" << x1 << endl;
        } else {
            x1 = (-b + sqrt(discr)) / 2 / a;
            x2 = (-b - sqrt(discr)) / 2 / a;
            cout << "x1=" << x1 << ", x2=" << x2 << "\n" << endl;
        }
    }
}

int main(int argc, char **argv) {

    cout << "Enter koefs: ";
    double A, B, C;
    cin >> A >> B >> C;

    quadraticEq(A, B, C);

    return 0;
}
