/*
 * (с) 2014, Владимир Стадник, программа 1.1
 * Написано и протестировано в Fedora GNU/Linux
 * ********************************************
 * Программа считывает из стандартного потока ввода строку
 * (одномерный массив символов) и выводит ее в стандартный
 * поток вывода.
 */

#include <iostream>
using namespace std;

int main(int argc, char **argv) {

    int const STRLEN = 256;
    char str[STRLEN];
    
    cout << "Enter a string: ";
    cin.getline(str, STRLEN);
    cout << "The string is \"" << str << "\"\n" << endl;

    return 0;
}
