/*
 * (с) 2014, Владимир Стадник, программа 1.6
 * Написано и протестировано в Fedora GNU/Linux
 * ********************************************
 * Subprog for the solution of a system of two linear equations
 * with two unknowns in real numbers.
 */

#include <iostream>
using namespace std;


void solveSystem(double a1, double b1, double c1,
                 double a2, double b2, double c2) {

    double x, y, delta = a1*b2 - a2*b1;

    if (delta) {
        x = (c1*b2 - c2*b1) / delta;
        y = (a1*c2 - a2*c1) / delta;

        cout << "x = " << x << "\t\ty = " << y << endl;
    }

    else if ((a1 && b1 && c1 && a2 && b2 && c2 && a1*c2 == a2*c1 && b1*c2 == b2*c1)
        || (a1 && b1 && c1 && !a2 && !b2 && !c2) || (!a1 && !b1 && !c1 && a2 && b2 && c2))
        cout << "Inf. solutions on R" << endl;

    else if (!b1 && !c1 && !b2 && !c2 && ((a1 && !a2) || (!a1 && a2)))
        cout << "x = 0\ty in real numbers" << endl;

    else if (!a1 && !c1 && !a2 && !c2 && ((b1 && !b2) || (!b1 && b2)))
        cout << "y = 0\tx in real numbers" << endl;

    else if (!a1 && !b1 && !a2 && !b2 && !c1 && !c2)
        cout << "Inf. solutions on R^2" << endl;

    else
        cout << "No solution" << endl;
}

int main(int argc, char **argv) {

    double a1, b1, c1, a2, b2, c2;

    cout << "Enter eq koefs: ";
    cin >> a1 >> b1 >> c1 >> a2 >> b2 >> c2;

    solveSystem(a1, b1, c1, a2, b2, c2);

    return 0;
}
