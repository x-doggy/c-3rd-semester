/*
 * (с) 2014, Владимир Стадник, программа 1.5
 * Написано и протестировано в Fedora GNU/Linux
 * ********************************************
 * Табулирование функции sin x в заданных пределах
 * и с заданным шагом.
 */

#include <iostream>
#include <iomanip>
#include <cmath>
using namespace std;


int main(int argc, char **argv) {

    double xMin=0.0, xMax=1.0, dx=0.005;

    cout << "Enter x minimum and x maximum: ";
    cin >> xMin >> xMax;
    cout << "Enter step: ";
    cin >> dx;
    
    cout << "\n---------------------------------------------------" << endl;
    cout << "      (c) 2014,  Tabing the function on C++ " << endl;
    cout << "  Program wrote in Geany IDE, Fedora GNU/Linux" << endl;
    cout << "---------------------------------------------------" << endl;
    cout << "    x     |         y       |" << endl;
    cout << "-----------------------------" << endl;

    for (double x=xMin; x<xMax+dx/2; x+=dx)
        cout << setw(10) << setprecision(4) << x
             << setw(18) << setprecision(9) << right << sin(x) << endl;

    return 0;
}
