/*
 * (с) 2014, Владимир Стадник, программа 1.7
 * Написано и протестировано в Fedora GNU/Linux
 * ********************************************
 * Decomposition of an exponent by Taylor's formula.
 * slag is a term
 * i is a counter of iterations on decomposition
 * ex is a result
*/

#include <iostream>
#include <cmath>
using namespace std;


int main(int argc, char **argv) {

    double x, ex = 1.0, eps, slag = 1.0;
    unsigned int i = 1;

    cout << "Enter x: ";
    cin >> x;
    cout << "Enter eps: ";
    cin >> eps;

    if (x == 0) {
        cout << "e^0 = 1" << endl << "Built-in exp(0) = " << exp(0) << endl;
        return 1;
    }

    do {
        slag *= x / i;
        ex += slag;
        i++;
    } while (fabs(slag) >= eps);

    cout << "\nAmount of elems in decomposition: " << i << endl
         << "e^" << x << " = " << ex << endl
         << "Built-in exp = " << exp(x) << endl;

    return 0;
}
