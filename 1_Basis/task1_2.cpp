/*
 * (с) 2014, Владимир Стадник, программа 1.2
 * Написано и протестировано в Fedora GNU/Linux
 * ********************************************
 * Программа, вводит три вещественных числа и выводит их произведение,
 * среднее арифметическое, а также сами эти числа в порядке возрастания.
 */

#include <iostream>
using namespace std;


void threeDoubleDigits(double a, double b, double c) {

    double mult, midAr;

    mult = a * b * c;
    midAr = (a + b + c) / 3;

    if (a > b && b > c)
        cout << c << ", " << b << ", " << a << endl;
    if (a > b && a > c && b < c)
        cout << b << ", " << c << ", " << a << endl;
    if (b > a && a > c)
        cout << c << ", " << a << ", " << b << endl;
    if (b > c && a < c)
        cout << a << ", " << c << ", " << b << endl;
    if (c > a && a > b)
        cout << b << ", " << a << ", " << c << endl;
    if (c > b && a < b)
        cout << a << ", " << b << ", " << c << endl;
    if (a == b && b > c)
        cout << c << ", " << b << ", " << a << endl;
    if (a == c && c > b)
        cout << b << ", " << a << ", " << c << endl;
    if (a == b && b == c)
        cout << c << ", " << b << ", " << a << endl;
    if (a == b && b < c)
        cout << a << ", " << b << ", " << c << endl;
    if (a == c && c < b)
        cout << a << ", " << c << ", " << b << endl;
    if (b == c && c < a)
        cout << c << ", " << b << ", " << a << endl;
    if (b == c && c > a)
        cout << a << ", " << b << ", " << c << endl;

    cout << "Multiply: " << mult << endl;
    cout << "Avegrage: " << midAr << "\n" << endl;
}

int main(int argc, char **argv) {

    double x, y, z;

    cout << "Enter 3 real numbers: ";
    cin >> x >> y >> z;

    threeDoubleDigits(x, y ,z);

    return 0;
}
