/*
 * (с) 2014, Владимир Стадник, все программы п. 2
 * Написано и протестировано в Fedora GNU/Linux
 * ********************************************
 * Процедуры работы с массивами
 */

#include <iostream>
using namespace std;



// Вывод массива
void printArray(const int a[], const int elems) {
    if (!a || elems < 0) return;
    
    for (int i=0; i<elems; i++)
        cout << a[i] << "  ";
    cout << endl;
}

// Заполнение
void fillArray(int a[], const int elems) {
    if (!a || elems < 0) return;
    
    for (int i=0; i<elems; i++) cin >> a[i];
}

// Сумма элементов
long int sumArray(const int a[], const int elems) {
    if (!a || elems < 0) return 0;
    
    long int sum = 0;
    for (int i=0; i<elems; i++)
        sum += a[i];

    return sum;
}

// Сумма нечётных
int evenArray(const int a[], const int elems) {
    if (!a || elems < 0) return 0;
    
    int res = 0;
    for (int i=0; i<elems; i++)
        if (!(a[i] % 2))
            res++;
    
    return res;
}

// Сумма элементов на промежутке [low; high]
int intervalArray(const int a[], const int elems, const int low, const int high) {
    if (!a || elems < 0 || low > high) return 0;
    
    int res = 0;
    for (int i=0; i<elems; i++)
        if (a[i] >= low && a[i] <= high)
           res++;

    return res;
}

// Проверка 2-х массивов на равенство
bool equalArrays(const int a[], const int size_a, const int b[], const int size_b) {
    if (!a || !b || size_a != size_b) return false;

    for (int i=0; i<size_a; i++)
        if (a[i] != b[i]) return false;

    return true;
}

// Проверка эл-тов массива на положительность
bool isPositiveArray(const int a[], const int elems) {
    if (!a || elems < 0) return false;

    for (int i=0; i<elems; i++)
        if (a[i] <= 0) return false;

    return true;
}

int main(int argc, char **argv) {

    const int size = 4;
    int A[size];
    cout << "Enter A [" << size << "]:" << endl; 
    fillArray(A, size);
    printArray(A, size);
    
    cout << "Sum of A : " <<  sumArray(A, size) << endl;
    cout << "Count of even elems : " << evenArray(A, size) << endl;
    cout << "Amount of elems in interval  [-10; 10] : " << intervalArray(A, size, -10, 10) << endl;
    cout << "Amount of elems in interval  [0; 3] : " << intervalArray(A, size, 0, 3) << endl;
    cout << "Amount of elems in interval  [10; 20] : " << intervalArray(A, size, 10, 20) << endl;
    cout << "Are all the elems in A positive? - " << isPositiveArray(A, size) << endl;
    
    cout << endl << "======================================" << endl;
    
    int B[size];
    cout << "Enter B [" << size << "]:" << endl;
    fillArray(B, size);

    printArray(B, size);
    
    cout << "Arrays A and B are" << ( equalArrays(A, size, B, size) ? "" : "n\'t" ) << " equal" << endl;
    cout << "Are the elems in B all positive? - " << isPositiveArray(B, size) << endl;

    return 0;
}
