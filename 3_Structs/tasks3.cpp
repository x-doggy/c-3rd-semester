/*
 * (с) 2014, Владимир Стадник, программа 3
 * Написано и протестировано в Fedora GNU/Linux
 * Набор функций для работы со структурой
 */

#include <iostream>
#include <cmath>
using namespace std;


struct Vector3D {
    double x, y, z;
};

const int MAX_VEC = 5;


// Выводит координаты вектора на экран
void doOutputVec(const Vector3D vec) {
    cout << "{" << vec.x << "; " << vec.y << "; " << vec.z << "}" << endl;
}

// Заполняет массив структур координатами вектора
void doInputVecArray(Vector3D vector[], const int vsize) {
    for (int i=0; i<vsize; i++) {
        cout << "Enter vector " << i << " coords: ";
        cin >> vector[i].x >> vector[i].y >> vector[i].z;
    }
}

// Выводит на экран массив векторов
void doOutputVecArray(const Vector3D vector[], const int vsize) {
    for (int i=0; i<vsize; i++)
        cout << "{"
             << vector[i].x << "; "
             << vector[i].y << "; "
             << vector[i].z << "}"
        << endl;
}

// Считает длину вектора
double countVectorSize(const Vector3D vec) {
    return sqrt(pow(vec.x, 2) +
                pow(vec.y, 2) +
                pow(vec.z, 2));
}

// Вычисляет скалярное произведение 2-х векторов
double countScalarMult(const Vector3D v1, const Vector3D v2) {
    return v1.x * v2.x +
           v1.y * v2.y +
           v1.z * v2.z;
}

// Вычисляет разность 2-х векторов
Vector3D makeSubstractVector(const Vector3D v1, const Vector3D v2) {
    Vector3D tmp;
    tmp.x = v2.x - v1.x;
    tmp.y = v2.y - v1.y;
    tmp.z = v2.z - v1.z;
    return tmp;
}

// Проверяет 2 вектора на коллинеарность
bool isVectorsCollinear(const Vector3D v1, const Vector3D v2) {
    return (v1.y * v2.z == v1.z * v2.y    &&
            v1.x * v2.z == v1.z * v2.x    &&
            v1.x * v2.y == v1.y * v2.x);
}

// Находит вектор максимального размера в массиве
double findMaxVectorSize(const Vector3D vec[], const int num) {
    double max = countVectorSize(vec[0]);
    for (int i=1; i<num; i++)
        if (countVectorSize(vec[i]) > max) max = countVectorSize(vec[i]);
    return max;
}

// Составляет вектор-линейную комбинацию векторов из массива, коеффициенты комбинации располагаются в отдельном массиве чисел
Vector3D linCombVectors(const Vector3D vec[], const double digits[], const int vnum) {
    Vector3D tmp {0, 0, 0};

    for (int i=0; i<vnum; i++) {
        tmp.x += digits[i] * vec[i].x;
        tmp.y += digits[i] * vec[i].y;
        tmp.z += digits[i] * vec[i].z;
    }

    return tmp;
}

// Находит индекс указанного вектора в массиве векторов
int findVector(const Vector3D vec[], const int num, const Vector3D toFind) {
    for (int i=0; i<num; i++)
        if (vec[i].x == toFind.x &&
            vec[i].y == toFind.y &&
            vec[i].z == toFind.z) return i;

    return -1;
}

// Вычисляет векторную сумму векторов из массива
Vector3D sumVectors(const Vector3D vec[], const int num) {
    Vector3D tmp {0, 0, 0};

    for (int i=0; i<num; i++) {
        tmp.x += vec[i].x;
        tmp.y += vec[i].y;
        tmp.z += vec[i].z;
    }

    return tmp;
}

// Проверяет вектор на его неравенство нулю
bool isNotNullVector(const Vector3D vect) {
    return vect.x && vect.y && vect.z;
}

// Вычисляет кол-во векторов, ортогональных указанному
int countOrtoVectors(const Vector3D vec[], const int vnum, const Vector3D vector) {
    int amount = 0;

    for (int i=0; i<vnum; i++)
        if (countScalarMult(vec[i], vector) == 0 &&
            isNotNullVector(vec[i]) &&
            isNotNullVector(vector)) amount++;

    return amount;
}


int main(int argc, char **argv) {

    Vector3D vec1, vec2, vec[MAX_VEC], vtmp;
    double numbers[MAX_VEC];
    
    cout << "Enter vector 1 coords: ";
    cin >> vec1.x >> vec1.y >> vec1.z;
    doOutputVec(vec1);
    
    cout << "Enter vector 2 coords: ";
    cin >> vec2.x >> vec2.y >> vec2.z;
    doOutputVec(vec2);
    
    cout << "1. Size of vector 1 = " << countVectorSize(vec1) << endl;
    cout << "1. Size of vector 2 = " << countVectorSize(vec2) << endl;
    
    cout << "2. Scalar multiplication of this vectors = " << countScalarMult(vec1, vec2) << endl;
    
    cout << "3. Substraction of this vectors: ";
    doOutputVec(makeSubstractVector(vec1, vec2));

    cout << "4. Is this vectors collinear? - " << isVectorsCollinear(vec1, vec2) << endl;

    cout << "\n==================================\n" << endl;

    cout << "5. Arrays of vectors\n>>" << endl;
    
    doInputVecArray(vec, MAX_VEC);
    doOutputVecArray(vec, MAX_VEC);
    cout << "6. Maximum vector size of array: " << findMaxVectorSize(vec, MAX_VEC) << endl;

    cout << "8. Sum of vectors in array: ";
    doOutputVec(sumVectors(vec, MAX_VEC));

    cout << "10. Amount of ortogonal vectors in array to vec1: " << endl
         << countOrtoVectors(vec, MAX_VEC, vec1) << endl
         << "10. Amount of ortogonal vectors in array to vec2: " << endl
         << countOrtoVectors(vec, MAX_VEC, vec2) << endl;

    cout << " << Array of numbers [" << MAX_VEC << "]" << endl;
    for (int j=0; j<MAX_VEC; j++) cin >> numbers[j];

    cout << "9. Linear combination of vec: " << endl;
    doOutputVec(linCombVectors(vec, numbers, MAX_VEC));


    cout << "7. << Vector to find" << endl;
    cin >> vtmp.x >> vtmp.y >> vtmp.z;
    cout << "Found [" << findVector(vec, MAX_VEC, vtmp) << "] vector in vec" << endl;

    return 0;
}
