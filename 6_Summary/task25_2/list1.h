#ifndef _LIBLIST1_
#define _LIBLIST1_

#include <iostream>
#include <fstream>
#include <limits>
typedef double elem_t;

// Errors structs
struct exNullListPointer{
    char const *what() const {
        return "List is nullptr!";
    }
};
struct exListIsEmpty {
    char const *what() const {
        return "List is empty";
    }
};
struct exFileNotExists{
    char const *what() const {
        return "File not exists!";
    }
};
struct exFileBadElem{
    char const *what() const {
        return "";
    }
};
struct exBadInputValue {
    char const *what() const {
        return "Bad value was entered!";
    }
};

elem_t readValueSecurely(std::istream &s = std::cin);

// Struct for list element
struct ListElem {
    elem_t     data;
    ListElem  *next;
};


ListElem* create();                                        // Create list
bool      is_end(const ListElem*, const ListElem*);        // Is end in list?
bool      is_list_empty(const ListElem*);                  // Is list empty?

void      to_begin(ListElem*, ListElem*&);                 // Put current to begin
void      to_next(ListElem*&);                             // Put current to next
void      to_end(ListElem*, ListElem*&);                   // Put current to end

elem_t    getl(ListElem*, ListElem*);                      // Get data into current
void      putl(ListElem*, ListElem*, const elem_t);        // Put new data into current

void      ins(ListElem*, ListElem*, const elem_t);         // Insert new elem after current
elem_t    ext(ListElem*, ListElem*);                       // Extracting elem with deleting buf
void      del(ListElem*&, ListElem*&);                     // Delete current

void      print(ListElem*, ListElem*);                     // Print list on screen
int       len(const ListElem*);                            // Length of list (amount of elems without buf)

void      clear(ListElem*&);                               // Delete all elems without buf
void      destroy(ListElem*&);                             // Delete all elems with buf
void      fwrite(const ListElem*, const char*);            // Write list in file
ListElem* fread(ListElem*&, const char*);                  // Read list from file

bool      is_nondecrease(const ListElem*);                 // Is list non-decreased?
void      ins_into_nondecr(ListElem*, const elem_t);       // Insert elem into non-decreased list
void      swap(ListElem*&, ListElem*&, ListElem*&);        // Swapping 2 elems
void      sort(ListElem*&);                                // Sorting list
ListElem* merge(ListElem*, ListElem*);                     // Merging list

#endif
