#include "list1.h"

elem_t readValueSecurely(std::istream &s) {
    elem_t n;
    if (!( s >> n )) {
        s.clear();
        s.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        throw exBadInputValue();
    }
    return n;
}

// Creating list
ListElem* create() {
    ListElem *buf = new ListElem;
    buf->data = 0;
    buf->next = buf;
    return buf;
}

// Is folowing to curr equal buf?
bool is_end(const ListElem *buf, const ListElem *curr) {
    if (!buf || !curr) throw exNullListPointer();
    return curr->next == buf;
}

// Is list empty?
bool is_list_empty(const ListElem *buf) {
    if (!buf) throw exNullListPointer();
    return buf->next == buf;
}

// Go current to begin
void to_begin(ListElem *buf, ListElem *&curr) {
    if (!buf) throw exNullListPointer();
    curr = buf;
}

// Go current forward
void to_next(ListElem *&curr) {
    if (!curr) throw exNullListPointer();
    curr = curr->next;
}

// Go current to end
void to_end(ListElem *buf, ListElem *&curr) {
    if (!buf) throw exNullListPointer();
    to_begin(buf, curr);
    while (!is_end(buf, curr))
        to_next(curr);
}

// Amount of elems in list
int len(const ListElem *buf) {
    if (!buf) throw exNullListPointer();
    if (is_list_empty(buf)) return 0;
    int a = 0;
    for (ListElem *p=buf->next; p!=buf; p=p->next)
        a++;
    return a;
}

// Inserting into current position in list
void ins(ListElem *buf, ListElem *curr, const elem_t x = elem_t()) {
    if (!buf || !curr) throw exNullListPointer();
    ListElem *elem = new ListElem;
    elem->data = x;
    elem->next = curr->next;
    curr->next = elem;
}

// Deleting "curr" from "buf"
void del(ListElem *&buf, ListElem *&curr) {
    if (!buf || !curr) throw exNullListPointer();
    if (buf == curr) return;

    if (is_end(buf, curr) && len(buf) == 1) { // If only one exists
        buf->next = buf;
        delete curr;
    } else if (is_end(buf, curr) && len(buf) > 1) { // If we delete elem in the end
        ListElem *tmp = buf->next;
        while (tmp->next->next != buf) tmp = tmp->next; // Know elem that prev for curr
        tmp->next = buf;
        delete curr;
    } else { // If elem in the middle
        ListElem *tmp = curr->next;
        curr->data = tmp->data;
        curr->next = tmp->next;
        delete tmp;
    }
}

// Getting data from curr
elem_t getl(ListElem *buf, ListElem *curr) {
    if (!buf || !curr) throw exNullListPointer();
    if (is_list_empty(buf)) throw exListIsEmpty();
    return curr->data;
}

// Replace data in current
void putl(ListElem *buf, ListElem *curr, const elem_t x = elem_t()) {
    if (!buf || !curr) throw exNullListPointer();
    if (is_list_empty(buf)) throw exListIsEmpty();
    curr->data = x;
}

// Extracting data with deleting elem
elem_t ext(ListElem *buf, ListElem *curr) {
    if (!buf || !curr) throw exNullListPointer();

    ListElem *p = curr->next;
    curr->next = p->next;
    delete p;
    return p->data;
}

void print(ListElem *buf, ListElem *cur) {
    std::cout << "\n-----------------------------\n";
    if (!buf) throw exNullListPointer();
    if (is_list_empty(buf)) throw exListIsEmpty();

    for (ListElem *p=buf->next; p!=buf; p=p->next)
        if (p == cur)
          std::cout << "->|" << p->data << "|";
        else 
          std::cout << "->" << p->data;
    std::cout << std::endl;
}

// Clear list
void clear(ListElem *&buf) {
    if (!buf) throw exNullListPointer();
    if (is_list_empty(buf)) throw exListIsEmpty();

    ListElem *p = buf->next;
    while (!is_end(buf, p)) {
        buf->next = p->next;
        delete p;
        p = buf->next;
    }
    delete p;
    buf->next = buf;
}

// Destroy list
void destroy(ListElem *&buf) {
    if (!buf) return;
    clear(buf);
    delete buf;
    buf = nullptr;
}

// Write list into file
void fwrite(const ListElem *buf, const char *fname) {
    if (!buf) throw exNullListPointer();
    if (is_list_empty(buf)) throw exListIsEmpty();

    std::ofstream fout(fname);
    if (!fout) {
        fout.close();
        throw exFileNotExists();
    }

    ListElem *p = buf->next;
    while (!is_end(buf, p) && fout.good()) {
        fout << p->data << std::endl;
        p = p->next;
    }
    fout.close();
}

// Creating new list from file
ListElem* fread(ListElem *&buf, const char *fname) {
    if (!fname) throw exNullListPointer();
    if (!buf) buf = create();

    std::ifstream fin(fname);
    if (!fin) {
        fin.close();
        throw exFileNotExists();
    }

    if (!is_list_empty(buf)) clear(buf);
    ListElem *p = buf->next;
    while (!fin.eof() && fin.good()) {
        elem_t x;
        if (!(fin >> x)) {
            fin.clear();
            if (!fin.eof())
                fin.ignore(fin.rdbuf()->in_avail());
            throw exFileBadElem();
        }
        ins(buf, p, x);
        to_next(p);
    }

    fin.close();
    return buf;
}


// Specific functions

// task "a"
bool is_nondecrease(const ListElem *buf) {
    if (!buf) throw exNullListPointer();
    if (is_list_empty(buf)) return false;

    ListElem *p = buf->next;
    while (!is_end(buf, p)) {
        if (p->next->data < p->data) return false;
        p = p->next;
    }
    return true;
}

// task "v"
void ins_into_nondecr(ListElem *buf, const elem_t x = elem_t()) {
    if (!buf) throw exNullListPointer();
    if (!is_nondecrease(buf)) return;

    ListElem *p = new ListElem;
    p->data = x;
    p->next = p;

    if (x < buf->next->data) {
        p->next = buf->next;
        buf->next = p;
    } else {
        ListElem *cur = buf->next;
        while (!is_end(buf, cur)) {
            if (cur->next->data > x) {
                break;
            }
            to_next(cur);
        }
        p->next = cur->next;
        cur->next = p;
    }
}

void swap(ListElem *&buf, ListElem *&x1, ListElem *&x2) {
    if (!buf || !x1 || !x2) throw exNullListPointer();
    elem_t tmp = x1->data;
    x1->data = x2->data;
    x2->data = tmp;
}

// Sorting bubbles
void sort(ListElem *&buf) {
    if (!buf) throw exNullListPointer();
    if (is_list_empty(buf)) throw exListIsEmpty();
    if (buf->next->next == buf) return;

    for (ListElem *p1=buf->next; p1!=buf; p1=p1->next) {
        for (ListElem *p2 = buf->next; p2 != buf; p2 = p2->next) {
            if (p1->data < p2->data)
                swap(buf, p1, p2);
        }
    }
}

// Merging two lists
ListElem* merge(ListElem *buf1, ListElem *buf2) {
    if (!buf1 || !buf2) throw exNullListPointer();
    if (is_list_empty(buf1) || is_list_empty(buf2)) {
        if (is_list_empty(buf1) && !is_list_empty(buf2))
            return buf2;
        if (is_list_empty(buf2) && !is_list_empty(buf1))
            return buf1;
        throw exListIsEmpty();
    }
    ListElem *p1 = buf1->next, // Iterator of 1st list
             *p2 = buf2->next, // Iterator of 2nd list
             *bufr = create(), // New list is result of merging
             *curr = bufr;     // Iterator of new list
    while (p1 != buf1 && p2 != buf2) {
            if (p1->data < p2->data) {
                ins(bufr, curr, p1->data);
                to_next(p1);
                to_next(curr);
            } else {
                ins(bufr, curr, p2->data);
                to_next(p2);
                to_next(curr);
            }
    }
    if (p1 != buf1) {
        for (ListElem *q = p1; q != buf1; q = q->next) {
            ins(bufr, curr, q->data);
            to_next(curr);
        }
    }
    if (p2 != buf2) {
        for (ListElem *q = p2; q != buf2; q = q->next) {
            ins(bufr, curr, q->data);
            to_next(curr);
        }
    }
    return bufr;
}
