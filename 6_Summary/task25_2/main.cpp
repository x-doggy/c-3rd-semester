/* (c) 2014, Vladimir Stadnik, program 6.2
 * Written and tested under Debian GNU/Linux 8 Jessie
 *
 * 39. Implement a list of real numbers. Write a function:
 *  a. check whether the list ordered by non-decreasing.
 *  b. sort the list in increasing order.
 *  c. merge two sorted lists by nondecreasing lists
 *  d. insert numbers in non-decreasing list with keeping list orderly.
 */

#include "list1.h"
using namespace std;


inline void print_menu() {
    cout << "-----------------------------" << endl
         << "1.  Insert elem to begin" << endl
         << "2.  Insert elem to end" << endl
         << "3.  Insert elem after current" << endl
         << "-----------------------------" << endl
         << "4.  Go to begin" << endl
         << "5.  Go forward" << endl
         << "6.  Delete current" << endl
         << "7.  Replace current" << endl
         << "-----------------------------" << endl
         << "8.  Write list to file" << endl
         << "9.  Read list from file" << endl
         << "10. Clear list" << endl
         << "-----------------------------" << endl
         << "11. Insert elem into non-decreased list" << endl
         << "12. Sort list" << endl
         << "13. Merge lists" << endl << endl;
}

int main(int argc, char **argv) {
    int key;
    ListElem *list = create(), // Main list
             *current = list;  // Iterator of list

    do {
        // Output list
        try {
            print(list, current);
            cout << "[" << len(list) << "]\t" << (is_nondecrease(list) ? "OK" : "bad") << " decreased!" << endl;
        } catch (exNullListPointer const &err) {
            cerr << err.what() << endl;
        } catch (exListIsEmpty const &err) {
            cerr << err.what() << endl;
        }
        
        print_menu();
        cout << "Enter your choose: ";
        
        try {
            key = (int) readValueSecurely();
        }
        catch (exBadInputValue const &err) {
            cerr << err.what() << endl;
            return -1;
        }

        switch (key) {

            case 1: {
                cout << "Enter value to ins: ";
                elem_t x;
                try {
                    x = readValueSecurely();
                } catch (exBadInputValue const &err) {
                    cerr << err.what() << endl;
                    break;
                }
                to_begin(list, current);
                ins(list, current, x);
                to_next(current);
                break;
            }

            case 2: {
                cout << "Enter value to ins: ";
                elem_t x;
                try {
                    x = readValueSecurely();
                } catch (exBadInputValue const &err) {
                    cerr << err.what() << endl;
                    break;
                }
                to_end(list, current);
                ins(list, current, x);
                to_next(current);

                break;
            }

            case 3: {
                cout << "Enter value to ins: ";
                elem_t x;
                try {
                    x = readValueSecurely();
                } catch (exBadInputValue const &err) {
                    cerr << err.what() << endl;
                    break;
                }
                ins(list, current, x);
                to_next(current);
                break;
            }

            case 4: {
                to_begin(list, current);
                break;
            }

            case 5: {
                to_next(current);
                break;
            }

            case 6: {
                del(list, current);
                break;
            }

            case 7: {
                cout << "Enter value to replace: ";
                elem_t x;
                try {
                    x = readValueSecurely();
                } catch (exBadInputValue const &err) {
                    cerr << err.what() << endl;
                    break;
                }

                try {
                    putl(list, current, x);
                } catch (exListIsEmpty const &err) {
                    cerr << err.what() << endl;
                }

                break;
            }

            case 8: {
                if (is_list_empty(list) || !list) {
                    cerr << "List is empty! Noting to write!" << endl;
                    break;
                }
                
                cout << "Enter filename to save list: ";
                char filename[255];
                cin.ignore();
                cin.getline(filename, 255);

                try {
                    fwrite(list, filename);
                    cout << "Successfully wrote to file!" << endl;
                } catch (exNullListPointer const &err) {
                    cerr << err.what() << endl;
                } catch (exListIsEmpty const &err) {
                    cerr << err.what() << endl;
                } catch (exFileNotExists const &err) {
                    cerr << err.what() << endl;
                }

                break;
            }

            case 9: {
                cout << "Enter filename to read list: ";
                char filename[255];
                cin.ignore();
                cin.getline(filename, 255);

                try {
                    fread(list, filename);
                    cout << "File read successfully!" << endl;
                } catch(exNullListPointer const &err) {
                    cerr << err.what() << endl;
                } catch (exListIsEmpty const &err) {
                    cerr << err.what() << endl;
                } catch (exFileNotExists const &err) {
                    cerr << err.what() << endl;
                } catch (exFileBadElem const &err) {
                    cerr << err.what() << endl;
                }

                break;
            }

            case 10: {
                try {
                    clear(list);
                } catch (exNullListPointer const &err) {
                    cerr << err.what() << endl;
                } catch (exListIsEmpty const &err) {
                    cerr << err.what() << endl;
                }
                
                current = list;

                break;
            }

            case 11: {
                if (!is_nondecrease(list)) {
                    cout << "List sorted incorrect!" << endl;
                    break;
                }
                
                cout << "Enter elem you want to insert: ";
                elem_t x;
                try {
                    x = readValueSecurely();
                } catch (exBadInputValue const &err) {
                    cerr << err.what() << endl;
                    break;
                }
                
                try {
                    ins_into_nondecr(list, x);
                } catch (exNullListPointer const &err) {
                    cerr << err.what() << endl;
                }

                break;
            }

            case 12: {
                try {
                    sort(list);
                } catch (exNullListPointer const &err) {
                    cerr << err.what() << endl;
                } catch (exListIsEmpty const &err) {
                    cerr << err.what() << endl;
                }

                break;
            }

            case 13: {
                if (!is_list_empty(list) && !is_nondecrease(list)) {
                    cerr << "List sorted incorrect!" << endl;
                    break;
                }
                
                ListElem *llist = create(),
                         *lcurrent = llist;

                ins(llist, lcurrent, -5);
                to_end(llist, lcurrent);
                ins(llist, lcurrent, -4);
                to_end(llist, lcurrent);
                ins(llist, lcurrent, -3);
                to_end(llist, lcurrent);
                ins(llist, lcurrent, 0);
                to_end(llist, lcurrent);
                ins(llist, lcurrent, 1);
                to_end(llist, lcurrent);
                ins(llist, lcurrent, 2);
                to_end(llist, lcurrent);
                ins(llist, lcurrent, 3);
                to_end(llist, lcurrent);
                ins(llist, lcurrent, 5);
                to_end(llist, lcurrent);
                ins(llist, lcurrent, 7);
                to_end(llist, lcurrent);

                ListElem *merged = 0, *mcurrent = 0;

                try {
                    merged = merge(list, llist);
                    mcurrent = merged;
                } catch (exNullListPointer const &err) {
                    cerr << err.what() << endl;
                } catch (exListIsEmpty const &err) {
                    cerr << err.what() << endl;
                }

               try {
                    print(merged, mcurrent);
                } catch (exNullListPointer const &err) {
                    cerr << err.what() << endl;
                } catch (exListIsEmpty const &err) {
                    cerr << err.what() << endl;
                }

                break;
            }

            default: break;
        }

    } while (key < 14);

    destroy(list);
    cout << "OK!" << endl;

    return 0;
}
