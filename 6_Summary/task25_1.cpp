/* (c) 2015, Vladimir Stadnik, program 6.1
 * Written and tested on Windows XP SP2
 * Optionally tested in Kubuntu 14.04.1
 *
 *
 * REQUIREMENTS:
 * The program must implement parsing the command line. If you run the
 * the program with parameters, the first of them is the name of the file
 * from which is necessary to take the source data. If no parameter is passed,
 * then enter is carried out from the standard input stream.
 *
 * The result should be a function (distinct from the main() function),
 * a crucial task. The source data (as a rules, the matrix and its
 * dimensions) are passed to the function as parameters.
 * The main() function should be a demonstration of the capabilities of written program.
 * Optionally you can enter helper functions.
 *
 *
 * THE TASK:
 * Beginning at element a_11 bypass on N by N matrix a square spiral,
 * printing items in order.
 */

#include <iostream>
#include <iomanip>
#include <fstream>
#include <limits>
using namespace std;

typedef int elem_t;

struct exBadSpiralParams {
    char const *what() const {
        return "Incorrect function params!";
    }
};
struct exBadCreateParams {
    char const *what() const {
        return "Incorrect size of array!";
    }
};
struct exBadInputValue {
    char const *what() const {
        return "Bad value was entered!";
    }
};

/**
 *
 * @param s input stream to read from
 * @return n value we read
 */
elem_t readValueProperly(istream &s = cin) {
    elem_t n = 0;
    if (!( s >> n )) {
        s.clear();
        s.ignore(numeric_limits<streamsize>::max(), '\n');
        throw exBadInputValue();
    }
    return n;
}

/**
 *
 * @param mx matrix we print for
 * @param size of matrix
 */
void printMatrixBySpiral(elem_t ** const mx, int const size) {
    if (!mx || size  <= 0) throw exBadSpiralParams();

    for (int p = 0; p < size / 2; p++) {                 // the number of spiral turns
        for (int j = p; j <= size - p - 1; j++)          // top row coil p
            cout << setw(4) << mx[p][j];
        for (int i = p + 1; i < size - p; i++)           // right row coil p
            cout << setw(4) << mx[i][size - p - 1];
        for (int j = size - p - 2;  j>= p; j--)           // bottom row coil p
            cout << setw(4) << mx[size - p - 1][j];
        for (int i = size - p - 2; i >= p + 1; i--)          // left row coil p
            cout << setw(4) << mx[i][p];
    }

    if (size % 2)
        cout << setw(4) << mx[size / 2][size / 2] << endl;
}

/**
 *
 * @param size a number of elements in future matrix
 * @return pointer by pointer by matrix
 */
elem_t** createArr(int const size) {
    if (size <= 0) throw exBadCreateParams();

    elem_t **arr = nullptr;
    int i = 0;
    try {
        arr = new elem_t*[size];
        // Array's memory allocation
        for (; i<size; i++) {
            arr[i] = new elem_t[size];
        }
    } catch (bad_alloc const &err) {
        for (int j=0; j<i; j++)
            delete[] arr[j];
        delete[] arr;
        throw err;
    }

    return arr;
}

int main(int argc, char **argv) {

    switch (argc) {

    case 1: {        // Standard work with array

        cout << "Enter array\'s size: ";
        int arrsize;
        try {
            arrsize = readValueProperly();
        }
        catch (exBadInputValue const &err) {
            cerr << err.what() << endl;
            return -1;
        }

        elem_t** arr = nullptr;

        // Creating array
        try {
            arr = createArr(arrsize);
        }
        catch (exBadCreateParams const &err) {
            cerr << err.what() << endl;
            return -1;
        }

        // Array's completion
        for (int i=0; i<arrsize; i++)
            for (int j=0; j<arrsize; j++) {
                cout << "Enter arr[" << i << ", " << j << "] = ";
                try {
                    arr[i][j] = readValueProperly();
                }
                catch (exBadInputValue const &err) {
                    cerr << err.what() << endl;
                    return -1;
                }
            }

        // Trying to launch procedure
        try {
            printMatrixBySpiral(arr, arrsize);
        }
        catch (exBadSpiralParams const &err) {
            cerr << err.what() << endl;
            return -1;
        }

        // Freeing memory back into heap
        for (int i=0; i<arrsize; i++) {
            delete[] arr[i];
        }

        delete[] arr;
        break;
    }

    case 2: {        // Work with array by file streams

        ifstream fin(argv[1]);
        if (!fin) {
            cerr << "File doesn\'t exists!" << endl;
            return -1;
        }

        int farrsize = 0;
        try {
            farrsize = readValueProperly(fin);
        }
        catch (exBadInputValue const &err) {
            cerr << err.what() << endl;
            return -1;
        }

        elem_t **farr = nullptr;

        // Creating array
        try {
            farr = createArr(farrsize);
        }
        catch (exBadCreateParams const &err) {
            cerr << err.what() << endl;
            return -1;
        }

        // Array's completion
        for (int i=0; i<farrsize; i++)
            for (int j=0; j<farrsize; j++)
                try {
                    farr[i][j] = readValueProperly(fin);
                }
                catch (exBadInputValue const &err) {
                    cerr << err.what() << endl;
                    return -1;
                }

        // Trying to launch procedure
        try {
            printMatrixBySpiral(farr, farrsize);
        }
        catch (exBadSpiralParams const &err) {
            cerr << err.what() << endl;
            return -1;
        }

        // Freeing memory back into heap
        for (int i=0; i<farrsize; i++) {
            delete[] farr[i];
        }
        delete[] farr;
        fin.close();

        break;
    }

    case 3: {     // Writing array into file

        ofstream fout(argv[1]);
        if (!fout) {
            cerr << "File doesn\'t exists!" << endl;
            return -1;
        }

        int ffarrsize = atoi(argv[2]);
        if (ffarrsize <= 0) {
            cerr << "Incorrect array\'s size!" << endl;
            return -1;
        }

        fout << ffarrsize << endl;   // Write array's size into file

        elem_t tmp = 0;
        for (int i=0; i<ffarrsize; i++)
            for (int j=0; j<ffarrsize; j++) {
                cout << "Enter [" << i << "][" << j << "]: ";
                try {
                    tmp = readValueProperly();
                }
                catch (exBadInputValue const &err) {
                    cerr << err.what() << endl;
                    return -1;
                }
                fout << tmp << endl;
            }

        fout.close();

        break;

    }

    default: break;
    }

    return 0;
}
