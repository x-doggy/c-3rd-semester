/*
 * @author Владимир Стадник, <x-doggy@ya.ru>
 * Написано и протестировано в Fedora GNU/Linux
 * **********
 * Для каждого символа латинского алфавита находится число его вхождений в строку.
 */

#include <iostream>
#include <cstring>
using namespace std;


int *latin_entries(char const *s) {
    int *nums = new int[52];

    for (int i=0; s[i]; i++) {
        if (isalpha(s[i])) {
            if (s[i] >= 'A' && s[i] <= 'Z')
                nums[s[i] - 'A']++;
            else if (s[i] >= 'a' && s[i] <= 'z')
                nums[s[i] - 'a' + 26]++;
        }
    }

    return nums;
}

int main(int argc, char **argv) {

    cout << "----- N. 10 -----" << endl;

    char str[101];
    cout << "Enter string: ";
    cin.getline(str, 101);

    int *entries = latin_entries(str);

    for (char i='A'; i<='Z'; i++)
        cout << i << " ";
    cout << endl;
    for (int i=0; i<26; i++)
        cout << entries[i] << " ";
    cout << endl << endl;
    for (char i='a'; i<='z'; i++)
        cout << i << " ";
    cout << endl;
    for (int i=26; i<52; i++)
        cout << entries[i] << " ";
    cout << endl;

    delete[] entries;

    return 0;
}
