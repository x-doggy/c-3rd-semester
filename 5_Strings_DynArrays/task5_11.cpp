/*
 * @author Владимир Стадник, <x-doggy@ya.ru>
 * Написано и протестировано в Fedora GNU/Linux
 * **********
 * Создание строки, полученной из исходной удалением каждого второго символа.
 */

#include <iostream>
#include <cstring>
using namespace std;


int main(int argc, char **argv) {

    cout << "----- N. 11 -----" << endl;

    const int CHR_MAX = 256;

    char *s = new char[CHR_MAX];
    cout << "Enter string: ";
    cin.getline(s, CHR_MAX);

    const int len = strlen(s);

    char *r = new char[(len + 1) / 2];

    int j = 0;
    for (int i=0; i<len; i+=2)
        r[j++] = s[i];

    r[(len + 1) / 2] = 0;

    cout << "Result str: " << r << endl;

    delete[] r;
    delete[] s;

    return 0;
}
