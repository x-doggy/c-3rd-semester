/*
 * Результаты олимпиады представлены в виде массива строк вида
 *      Фамилия И.О. номер_задачи баллы_за_задачу
 * Номер задачи и баллы - целые числа (не более 100). Элементы строки разделены одним пробелом.
 * Итоговый балл участника есть сумма набранных им баллов за все задачи.
 * Победители - участники, показавшие максимальный результат (их может быть несколько).
 * Функция выводит на экран ФИО всех победителей.
 */

#include <iostream>
#include <cstring>
#include <cstdlib> // нас обманули, ф-ции atoi нет в string.h!
#include <limits>
using namespace std;


struct TeamElem { // структура "участник"
    char *surname; // фамилия
    char *initials; // инициалы
    int tasknum; // номер задачи
    int taskres; // результат
};


void printTeamWinners(const TeamElem *team, const int len) {
    if (len<=0 || !team) return;

    bool flag = false;
    int  diff_amount = 0;

    for (int i=0; i<len; i++) {
        for (int j=i-1; j>=0; j--)
            if (!strcmp(team[i].surname,  team[j].surname) &&
                !strcmp(team[i].initials, team[j].initials))
                flag = true;
        if (!flag)
            diff_amount++;
        flag = false;
    }

    TeamElem *res = new TeamElem[diff_amount];
    int k = 0;

    for (int i=0; i<len; i++) {
        for (int j=i-1; j>=0; j--)
            if (res[i].surname && res[i].initials &&
                !strcmp(team[i].surname,  team[j].surname) &&
                !strcmp(team[i].initials, team[j].initials))
                flag = true;
        if (!flag) {
            res[k].surname = team[i].surname;
            res[k].initials = team[i].initials;
            res[k].tasknum = team[i].tasknum;
            res[k].taskres = 0;
            for (int h=0; h<len; h++)
                if (!strcmp(res[k].surname,   team[h].surname) &&
                    !strcmp(res[k].initials,  team[h].initials))
                    res[k].taskres += team[h].taskres;
            k++;
        }
        flag = false;
    }

    int max = res[0].taskres;
    for (int i=1; i<len; i++)
        if (res[i].taskres > max && res[i].surname && res[i].initials)
            max = res[i].taskres;

    for (int i=0; i<len; i++)
        if (res[i].taskres == max && res[i].surname && res[i].initials)
            cout << i << ". " << res[i].surname << " " << res[i].initials << " " << res[i].taskres << endl;

    // Collect garbage
    delete[] res;
}


int main(int argc, char **argv) {

    const int CHR_MAX = 256;

    cout << "----- N. 15 -----" << endl;
    cout << "Enter a number of teammates: ";
    int TEAM_LEN;
    while (!(cin >> TEAM_LEN) || TEAM_LEN <= 0) {
        cin.clear();
        cin.ignore(numeric_limits<streamsize>::max(), '\n');
        cerr << "Incorrect input!" << endl;
        cout << "Try to enter again: ";
    }

    TeamElem *Team = new TeamElem[TEAM_LEN];

    for (int i=0; i<TEAM_LEN; i++) {
        Team[i].surname = new char[1];
        Team[i].surname[0] = 0;
        Team[i].initials = new char[1];
        Team[i].initials[0] = 0;
        Team[i].tasknum = 0;
        Team[i].taskres = 0;
    }

    cin.ignore();
    for (int i=0; i<TEAM_LEN; i++) {
        char *name = new char[CHR_MAX];

        cout << "Enter string [" << i << "] = ";
        cin.getline(name, CHR_MAX);

        // Если пустая строка, то пропуск
        if (!name) {
            cerr << "Incorrect common string!" << endl;
            delete[] name;
            continue;
        }

        name = strtok(name, " ");
        if (!name) {
            cerr << "Incorrect cutted string!" << endl;
            delete[] name;
            continue;
        }

        char *initials = strtok(0, " ");
        if (!initials || strlen(initials) != 4) {
            cerr << "Incorrect initials!" << endl;
            continue;
        }

        char *tasknum = strtok(0, " ");
        if (!tasknum || tasknum[0] == '-') {
            cerr << "Incorrect number of task!" << endl;
            continue;
        }

        char *taskres = strtok(0, " ");
        if (!taskres || taskres[0] == '-') {
            cerr << "Incorrect result!" << endl;
            continue;
        }

        Team[i].surname = name;
        Team[i].initials = initials;
        Team[i].tasknum = atoi(tasknum);
        Team[i].taskres  = atoi(taskres);
    }

    cout << endl << endl;
    printTeamWinners(Team, TEAM_LEN);


    // Collect garbage
    for (int i=0; i<TEAM_LEN; i++)
        delete[] Team[i].surname;
    delete[] Team;

    return 0;
}
