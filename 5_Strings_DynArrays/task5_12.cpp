/*
 * @author Владимир Стадник, <x-doggy@ya.ru>
 * Написано и протестировано в Fedora GNU/Linux
 * **********
 * На входе массив строк и его длина. Каждая строка имеет вид:
 *      Фамилия Имя Отчество
 * (разделяются одним пробелом).
 * Функция должна возвращать новый массив, состоящий из строк вида
 *      И.О. Фамилия.
 * **********
 * @param s исходная строка
 * @param leng кол-во элементов в массиве.
 */

#include <iostream>
#include <cstring>
using namespace std;


void fio(char **s, const int leng) {
    if (!s || leng <= 0) return;

    for (int i=0; i<leng; i++) {
        s[i] = strtok(s[i], " ");

        char *name = strtok(0, " ");
        if (!name)
            cout << "No name!" << endl;
        else {
            char *fathername = strtok(0, " ");
            if (!fathername) {
                cout << "No fathername!" << endl;

                char *tmp = new char[4];
                tmp[0] = ' ';
                tmp[1] = name[0];
                tmp[2] = '.';
                tmp[3] = 0;

                strcat(s[i], tmp);

                delete[] tmp;
                continue;
            } else {
                char *io = new char[6];
                io[0] = ' ';
                io[1] = name[0];
                io[2] = '.';
                io[3] = fathername[0];
                io[4] = '.';
                io[5] = 0;

                strcat(s[i], io);
                delete[] io;
            }
        }
    }
}

int main(int argc, char **argv) {

    cout << "----- N. 12 -----" << endl;
    
    cout << "Enter amount: ";
    int amount;
    cin >> amount;

    char **s = new char* [amount];

    cin.ignore();
    for (int i=0; i<amount; i++) {
        s[i] = new char[256];
        cout << "Enter surname, name and fathername [" << i << "]: ";
        cin.getline(s[i], 256);
    }

    fio(s, amount);

    for (int i=0; i<amount; i++)
        cout << "s[" << i << "] = \"" << s[i] << "\"" << endl;

    cout << endl;

    return 0;
}
