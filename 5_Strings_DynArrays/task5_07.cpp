/*
 * @author Владимир Стадник, <x-doggy@ya.ru>
 * Написано и протестировано в Fedora GNU/Linux
 * **********
 * Функция создания копии строки (с выделением памяти),
 * без обращения к библиотечным функциям.
 * **********
 * @param dest строка, в которую будет копирование.
 * @param src  строка-источник
 */

#include <iostream>
#include <cstring>
using namespace std;

// Procedure copy string src to string dest
void copystr(char *dest, char *src) {
    if (!src) return;
    
    if (!dest)
        dest = new char[strlen(src) + 1];
    
    while (*src)
        *dest++ = *src++;
    *dest = 0;
}

int main(int argc, char **argv) {

    cout << "----- N. 7 -----" << endl;

    const int CHR_MAX = 256;

    cout << "Enter string: ";
    char *str = new char[CHR_MAX];
    cin.ignore();
    cin.getline(str, CHR_MAX);

    char *copied = new char[CHR_MAX];
    copystr(copied, str);
    cout << "Result: \"" << copied << "\"" << endl << endl;

    delete[] copied;
    delete[] str;

    return 0;
}
