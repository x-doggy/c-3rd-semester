/*
 * @author Владимир Стадник, <x-doggy@ya.ru>
 * Написано и протестировано в Fedora GNU/Linux
 * **********
 * Функция получает на вход массив целых чисел и возвращает новый массив,
 * содержащий все четные числа, которые есть в исходном массиве.
 * Если таких чисел нет, функция возвращает нулевой указатель.
 * Длина нового массива должна в точности совпадать с количеством таких чисел
 * и возвращаться как выходной параметр.
 */

#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

int* chooseEven(const int *arr, int &len) {
    if (!arr || len <= 0) return 0;

    int even = 0;
    for (int i=0; i<len; i++)
        if (!(arr[i] % 2)) even++;

    if (!even) return 0;

    int *new_arr = new int[even];
    int ptr = 0;

    for (int i=0; i<len; i++)
        if (!(arr[i] % 2))
            new_arr[ptr++] = arr[i];

    len = ptr;

    return new_arr;
}

inline void usage() {
    cout <<
        "m   - заполнить массив вручную" << endl <<
        "n   - заполнить массив автоматически" << endl <<
        "r   - запустить алгоритм" << endl <<
        "p   - вывести массив" << endl <<
        "q   - выход" << endl <<
        "h   - вывести эту справку"
    << endl;
}

inline void printArr(int const *arr, int const len) {
    cout << "| ";
    for (int i=0; i<len; i++)
        cout << arr[i] << " | ";
    cout << endl;
}

int main(int argc, char **argv) {

    char key;
    int length = 0;
    int *ar = nullptr;

    cout << "Введите команду. Нажмите h для справки" << endl << endl;

    do {
        cout << "-> ";
        cin >> key;

        switch (key) {

            case 'm': {
                cout << "Length of array: ";
                cin >> length;

                delete[] ar;
                ar = new int[length];

                for (int i=0; i<length; i++) {
                    cout << "ar[" << i << "] = ";
                    cin >> ar[i];
                }

                printArr(ar, length);

                cout << "Array filled successfully!" << endl;
                break;
            }

            case 'n': {
                cout << "Length of array: ";
                cin >> length;

                delete[] ar;
                ar = new int[length];

                srand(static_cast<unsigned>(time(0)));
                for (int i=0; i<length; i++) {
                    ar[i] = rand() % 100 + 1;
                }

                printArr(ar, length);

                cout << "Array filled successfully!" << endl;
                break;
            }

            case 'r': {
                ar = chooseEven(ar, length);
                printArr(ar, length);

                break;
            }

            case 'p': {
                printArr(ar, length);
                break;
            }

            case 'h': {
                usage();
                break;
            }

        }
        
        cout << endl;

    } while (key != 'q');

    delete[] ar;

    return 0;
}
