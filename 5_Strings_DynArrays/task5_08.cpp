/*
 * @author Владимир Стадник, <x-doggy@ya.ru>
 * Написано и протестировано в Fedora GNU/Linux
 * 
 * **** N  8 *****
 * Построение по строке новой строки, которая получена из исходной заменой
 * символа '1' на подстроку "odin",
 * символа '2' на подстроку "dva" и
 * символа '3' на подстроку "tri".
 */

#include <iostream>
#include <cstring>
using namespace std;


char* replace_to_123(const char *s) {
    if (!s) return 0;

    int cnt1=0, cnt2=0, cnt3=0;

    for (int i=0; s[i]; i++)
        switch (s[i]) {
            case '1': cnt1++; break;
            case '2': cnt2++; break;
            case '3': cnt3++; break;
        }

    int len = strlen(s) + cnt1*4 + cnt2*3 + cnt3*3;
    int j=0;

    char *new_str = new char[len+1];

    for (unsigned i=0; i<strlen(s); i++) {

        switch (s[i]) {
            case '1':
                new_str[j++] = 'o';
                new_str[j++] = 'd';
                new_str[j++] = 'i';
                new_str[j++] = 'n';
                break;

            case '2':
                new_str[j++] = 'd';
                new_str[j++] = 'v';
                new_str[j++] = 'a';
                break;

            case '3':
                new_str[j++] = 't';
                new_str[j++] = 'r';
                new_str[j++] = 'i';
                break;

            default:
                new_str[j++] = s[i];
                break;
        }
    }

    new_str[len] = 0;
    return new_str;
}


int main(int argc, char **argv) {

    cout << "----- N. 8 -----" << endl;

    const int CHR_MAX = 1000;

    cout << "Enter string: ";
    char *c = new char[CHR_MAX];
    cin.getline(c, CHR_MAX);

    char *buf = replace_to_123(c);
    cout << "Result: \"" << buf << "\"" << endl << endl;

    delete[] c;
    delete[] buf;

    return 0;
}
