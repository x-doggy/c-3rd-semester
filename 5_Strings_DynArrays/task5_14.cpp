/*
 * @author Владимир Стадник, <x-doggy@ya.ru>
 * Написано и протестировано в Fedora GNU/Linux
 * **********
 * На входе массив целых чисел (возможно, повторяющихся). Функция строит новый массив,
 * содержащий все элементы исходного массива, но по одному разу.
 */


#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <ctime>
using namespace std;


int* deleteRepeated(const int *mas, const int size, int &cnt) {
    if (!mas || size <= 0) return 0;
    
    cnt = 0;
    bool flag = false;

    // вычисление кол-ва одинаковых элементов массива
    for (int i=0; i<size; i++) {
        for (int j=i-1; j>=0; j--)
            if (mas[i] == mas[j])
                flag = true;
        if (!flag) cnt++;
        flag = false;
    }

    // создание массива без одинаковых элементов
    int *res = new int[cnt], k=0;
    for (int i=0; i<size; i++) {
        for (int j=i-1; j>=0; j--)
            if (mas[i] == mas[j]) flag = true;
        if (!flag) res[k++] = mas[i];
        flag = false;
    }

    return res;
}

int main(int argc, char **argv) {

    cout << "----- N. 14 -----" << endl;

    cout << "Enter ARR LEN: ";
    int len = 0;
    cin >> len;
    
    int *a14 = new int[len];

    cout << "How do you want to fill array?" << endl
         << "1. Manual" << endl
         << "2. Pseudorandomly" << endl;

    int p;
    cin >> p;

    switch (p) {

        case 1: {
            for (int i=0; i<len; i++) {
                cout << "Enter a[" << i << "] = ";
                cin >> a14[i];
            }

            cout << endl << endl;
            break;
        }

        case 2: {
            srand(static_cast<unsigned>(time(0)));

            for (int i=0; i<len; i++) {
                a14[i] = rand() % 10 + 1;
                cout << setw(4) << a14[i];
            }

            cout << endl << endl;
            break;
        }

    } // closed switch p

    cout << endl << "Result:" << endl;
    int tmp = 0, *tmpmas = deleteRepeated(a14, len, tmp);

    for (int i=0; i<tmp; i++) cout << setw(4) << tmpmas[i];

    delete[] tmpmas;
    delete[] a14;

    return 0;
}
