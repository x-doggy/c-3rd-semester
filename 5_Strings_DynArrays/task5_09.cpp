/*
 * @author Владимир Стадник, <x-doggy@ya.ru>
 * Написано и протестировано в Fedora GNU/Linux
 * **********
 * В строке меняются местами первое и последнее слова.
 * Слова разделяются группами пробелов, которые возможны в начале и в конце строки.
 */

#include <iostream>
#include <cstring>
using namespace std;


char* swap_1st_and_last(char *s) {
    if (!s) return 0;

    char *res = new char[strlen(s) + 1];

    // First spaces
    unsigned pos_x1 = 0;
    while ((s[pos_x1]==' ' || s[pos_x1]=='\t') && pos_x1<strlen(s)) pos_x1++;

    // Following word after first spaces
    unsigned pos_x2 = pos_x1;
    while ((s[pos_x2]!=' ' || s[pos_x2]!='\t') && pos_x2<strlen(s)) pos_x2++;

    // Last spaces
    unsigned pos_y1 = strlen(s) - 1;
    while ((s[pos_y1]==' ' || s[pos_y1]=='\t') && pos_y1>0) pos_y1--;

    // Last word before last spaces
    unsigned pos_y2 = pos_y1;
    while ((s[pos_y2]!=' ' || s[pos_y2]!='\t') && pos_y2>0) pos_y2--;

    // If nothing to swap
    if ((!pos_x1 && !pos_y2) || (pos_x1 == pos_y2+1))
        return s;


    // Make word parts
    unsigned pos = 0;

    // Make first spaces
    if (pos_x1) for (unsigned i=pos; i<pos_x1; i++) res[pos++] = ' ';

    // Make last word
    for (unsigned i=pos_y2+1; i<pos_y1+1; i++) res[pos++] = s[i];

    // Make middle part of string
    for (unsigned i=pos_x2; i<pos_y2+1; i++) res[pos++] = s[i];

    // Make first word
    for (unsigned i=pos_x1; i<pos_x2; i++) res[pos++] = s[i];

    // Make last spaces
    if (pos_y1 != strlen(s)-1)
        for (unsigned i=0; i<strlen(s)-pos_y1-1; i++) res[pos++] = ' ';

    return res;
}

int main(int argc, char **argv) {

    cout << "----- N. 9 -----" << endl;
    
    char *s = new char[1000];
    cout << "Enter string: ";
    cin.getline(s, 1000);

    char *res = swap_1st_and_last(s);

    cout << "Result = " << res << endl;

    delete[] res;

    return 0;
}
