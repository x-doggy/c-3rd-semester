/* @author Владимир Стадник, <x-doggy@ya.ru>
 * Написано и протестировано в Kubuntu GNU/Linux 14.0
 * 
 * ***** N 16 - 20 *****
 *
 *  Дана структура
 *   struct Payment {
 *      char *Name; // ФИО человека в виде строки
 *      char *Date; // дата в виде строки формата dd.mm.yy
 *      int Sum;    // сумма платежа
 *   };
 * Следующие функции должны получать на вход массив структур типа Payment, длину массива и,
 * возможно, дополнительные параметры, указанные в задачах.
 *
 *
 * ***** N 16 *****
 * Функции создания массива из n структур, удаления массива, ввода и всех элементов массива.
 *
 *
 * ***** N 17 *****
 * На входе символ. Функция выводит на экран платежи всех людей, чья фамилия начинается
 * на указанный символ.
 *
 *
 * ***** N 18 *****
 * На входе число. Функция выводит на экран информацию о всех платежах, больше этого числа.
 *
 *
 * ***** N 19 *****
 * На входе строка с датой. Функция вычисляет суммарный платеж на эту дату.
 *
 *
 * ***** N 20 *****
 * Функция выводит названия месяцев, в которых не было ни одного платежа.
 */

#include <iostream>
#include <iomanip>
#include <cstring>
#include <cstdlib>
using namespace std;


struct Payment {
    char *name;
    char *date;
    int sum;
};

const int CHR_MAX = 256;


// N. 16
Payment* createPayment(const int amount) {
    if (amount <= 0) return 0;

    Payment *p = new Payment[amount];

    for (int i=0; i<amount; i++) {
        p[i].name = new char[CHR_MAX];
        p[i].date = new char[CHR_MAX];
        p[i].sum = 0;
    }

    return p;
}

void delPayment(Payment *&p, const int amount) {
    if (amount <= 0 || !p) return;

    for (int i=0; i<amount; i++) {
        delete[] p[i].name;
        delete[] p[i].date;
    }

    delete[] p;
}

void inpPayment(Payment *p, const int amount) {
    if (amount <= 0 || !p) return;

    for (int i=0; i<amount; i++) {
        cout << "Enter name [" << i << "] = ";
        cin.ignore();
        cin.getline(p[i].name, CHR_MAX);
        cout << "Enter date [" << i << "] = ";
        cin >> p[i].date;
        cout << "Enter sum [" << i << "] = ";
        cin >> p[i].sum;
    }
}

void printPayElem(const Payment *p, const int i) {
    if (!p || i<0) return;

    cout << ( (!p[i].name) ? "No name" : p[i].name ) << endl;
    cout << ( (!p[i].date) ? "No date" : p[i].date ) << endl;
    cout << p[i].sum << "\n--------------\n" << endl;
}

void printPayment(const Payment *p, const int amount) {
    if (!p || amount <= 0) return;

    for (int i=0; i<amount; i++) {
        cout << "--------------" << endl
             << " Struct " << i << ":" << endl
             << "--------------" << endl;

        printPayElem(p, i);
    }
}


// N. 17
void printPayElemsByChar(const Payment *p, const int amount, const char c) {
    if (!p || amount <= 0) return;

    for (int i=0; i<amount; i++)
        if (p[i].name[0] == c)
            printPayElem(p, i);
}


// N. 18
void printGreaterPays(const Payment *p, const int amount, const int m) {
    if (!p || amount <= 0) return;

    for (int i=0; i<amount; i++)
        if (p[i].sum > m)
            printPayElem(p, i);
}


// N. 19
void printSumPaysByDate(const Payment *p, const int amount, const char *d) {
    if (!p || amount <= 0) return;

    long s = 0;

    for (int i=0; i<amount; i++)
        if ( !strcmp(p[i].date, d) )
            s += p[i].sum;

    cout << "Sum pays by " << d << " = " << s << endl;
}


// N. 20
void printNoPaysByMonths(const Payment *p, const int amount) {
    if (!p || amount <= 0) return;

    const char strmon[12][4] = {"Jan", "Feb", "Mar",
                                "Apr", "May", "Jun",
                                "Jul", "Aug", "Sep",
                                "Oct", "Nov", "Dec"};

    bool months[12];
    for (int i=0; i<12; i++) months[i] = false;

    for (int i=0; i<amount; i++) {

        char *tmp = strtok(p[i].date, ".");
        if (!tmp) {
            //cout << "Incorrect date [" << i << "]!" << endl;
            continue;
        }

        tmp = strtok(0, ".");
        if (!tmp) {
            //cout << "Incorrect date [" << i << "]!" << endl;
            continue;
        }

        int mon = atoi(tmp);
        if (mon < 1 || mon > 12) {
            //cout << "Incorrect date [" << i << "]!" << endl;
            continue;
        }

        months[mon-1] = true;
    }

    for (int i=0; i<12; i++)
        if (!months[i])
            cout << setw(5) << strmon[i];

    cout << endl;
}


int main(int argc, char **argv) {

    int n;
    cout << "Enter amount of payments: ";
    cin >> n;

    if (n <= 0) {
        cout << "Incorrect amount!" << endl;
        return -1;
    }

    cout << "----- N. 16 -----" << endl;
    Payment *p = createPayment(n);
    inpPayment(p, n);
    printPayment(p, n);


    cout << "\n----- N. 17 -----" << endl;
    char c;
    cout << "Enter a char for name: ";
    cin >> c;

    printPayElemsByChar(p, n, c);


    cout << "\n----- N. 18 -----" << endl;
    int m;
    cout << "Enter a digit: ";
    cin >> m;

    printGreaterPays(p, n, m);


    cout << "\n----- N. 19 -----" << endl;
    char *d = new char[CHR_MAX];
    cout << "Enter a date: ";
    cin >> d;

    printSumPaysByDate(p, n, d);

    delete[] d;


    cout << "\n----- N. 20 -----" << endl;
    cout << "No payments was on:" << endl;

    printNoPaysByMonths(p, n);


    delPayment(p, n);

    return 0;
}
